# Caelum-Incognitum

[![Codacy Badge](https://api.codacy.com/project/badge/grade/2b0df83c67a24f2abc5c762e6db82c52)](https://www.codacy.com/app/lastmikoi/caelum-incognitum)
[![Codacy Coverage Badge](https://api.codacy.com/project/badge/coverage/2b0df83c67a24f2abc5c762e6db82c52)](https://www.codacy.com/app/lastmikoi/caelum-incognitum)
[![Codeship Status for lastmikoi/caelum-incognitum](https://codeship.com/projects/5c676b70-7477-0133-4e06-52f3970f70f1/status?branch=master)](https://codeship.com/projects/117647)
[![Requirements Status](https://requires.io/bitbucket/lastmikoi/caelum-incognitum/requirements.svg?branch=master)](https://requires.io/bitbucket/lastmikoi/caelum-incognitum/requirements/?branch=master)

Caelum-Incognitum is - rather will be - a multiplayer-oriented space survival
and exploration game with aspects of sandbox, procedural map generation,
resource gathering and trading.

Gather with friends or more accurately daredevil business partners to land onto,
explore and exploit one of many of the Solar System's asteroids, for adrenaline
and profit. Brave the dangers of Space from meteroid showers to oxygen shortage,
while keeping an eye on your station's income.

## Download

While no ready-to-use archive are available, feel free to clone the source from
[our BitBucket repository](https://bitbucket.org/lastmikoi/caelum-incognitum/)
and setup a development-like environment in order to run the game.

## Play Online

One of the main goals of the project is to provide an online server that will
allow anyone to connect using a widely-used client such as SSH and to play the
game without having to install the game.

## Contribute

Caelum-Incognitum, by its very broad and sandbox-y nature, always has room for
new features and improvement, and as such it is in dire need of contributors. We
are coordinating the development effort from a
[Trello board](https://trello.com/b/8HI0OnsN/caelum-incognitum), 
so make sure to check out what's on the Todolist if you feel like contributing
(you can ask write access to the Board by contacting one of its admins).

Caelum-Incognitum is using a double license, artistic works (such as pictures, 
sounds, or stories) are licensed under the
[Creative Commons Attribution ShareAlike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/) while the source code by itself is licensed under the
[GNU General Public License 3.0](https://www.gnu.org/licenses/gpl-3.0.html).
This means the code and the content of the game is free to use, modify, and
redistribute for any purpose whatsoever, but must however be shared using
the same or a compatible license.

[![CC-BY-SA-4.0](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)](https://creativecommons.org/licenses/by-sa/4.0/)
[![GNU GPLv3.0](https://www.gnu.org/graphics/gplv3-127x51.png)](https://www.gnu.org/licenses/gpl-3.0.html)

Please see [CONTRIBUTING.md](https://bitbucket.org/lastmikoi/caelum-incognitum/src/HEAD/CONTRIBUTING.md?at=master&fileviewer=file-view-default) for more details.

## Community

Discuss, debate and suggest ideas for the game in our IRC Channel:
  irc.freenode.net ; #caelum-incognitum
  http://webchat.freenode.net/?channels=#caelum-incognitum

Contribute, browse the source code or file a bug using our BitBucket repository:
  https://bitbucket.org/lastmikoi/caelum-incognitum

Watch the current development effort from our Trello board:
  https://trello.com/b/8HI0OnsN/caelum-incognitum