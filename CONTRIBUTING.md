# Contribute

Contributing to Caelum-Incognitum is as easy as contributing to other open-source projects, you just have to make your own fork of our repository, commit changes to it and then submit a pull request to get your changes merged back into the main repository.

Caelum-Incognitum is using a double license, artistic works (such as pictures, 
sounds, or stories) are licensed under the
[Creative Commons Attribution ShareAlike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/) while the source code by itself is licensed under the
[GNU General Public License 3.0](https://www.gnu.org/licenses/gpl-3.0.html).
This means the code and the content of the game is free to use, modify, and
redistribute for any purpose whatsoever, but must however be shared using
a the same or a compatible license. Any contribution you make to the project
will be also licensed under thoses conditions, which are irrevocable.

[![CC-BY-SA-4.0](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)](https://creativecommons.org/licenses/by-sa/4.0/)
[![GNU GPLv3.0](https://www.gnu.org/graphics/gplv3-127x51.png)](https://www.gnu.org/licenses/gpl-3.0.html)

## Guidelines

To make sure you don't come across any complex situations when it comes to using git, make sure to apply the following guidelines:

* Add this repository as an `upstream` remote.
* Keep your `master` branch clean. This means you can easily pull changes made to this repository into yours.
* Create a new branch for each new feature or set of related bug fixes.
* Never merge from your local branches into your `master` branch. Only update that by pulling from `upstream/master`.

## Code Style

When it comes to coding style, we are following the [PEP 0008, Style Guide for Python Code](https://www.python.org/dev/peps/pep-0008/) in order to keep the code readable for everyone. Make sure that your contributions are *pep8-compliant* before submitting them.

## Docstrings

We recommend documenting all your classes, methods and functions using [Docstrings](https://www.python.org/dev/peps/pep-0257/).

This specific section will get more details once we get a [Sphinx-based](http://sphinx-doc.org) API documentation generation process set up.

## Getting started

#### Setup your environment

*(This only needs to be done once.)*

1. Setup a fork of the repository.

2. Clone your fork locally.

        $ git clone git@bitbucket.org:USERNAME/caelum-incognitum.git caelum-incognitum

3. Add the `upstream` repository as a remote.

        $ cd caelum-incognitum
        $ git remote add -f upstream https://bitbucket.org/lastmikoi/caelum-incognitum.git

#### Update your `master` branch

1. Make sure you have your `master` branch checked out.

        $ git checkout master

2. Pull the changes from the `upstream/master` branch.

        $ git pull --ff-only upstream master

 * Note: If this gives you an error, it means you have committed directly to your local `master` branch. [Click here for instructions on how to fix this issue](#why-does-git-pull---ff-only-result-in-an-error).

#### Make your changes

0. Update your `master` branch, if you haven't already.

1. For each new feature or bug fix, create a new branch.

        $ git branch new_feature
        # Creates a new branch called "new_feature"
        $ git checkout new_feature
        # Makes "new_feature" the active branch

2. Once you've committed some changes locally, you need to push them to your fork.

        $ git push origin new_feature
        # origin was automatically set to point to your fork when you cloned it


3. Once you're finished working on your branch, and have committed and pushed all your changes, submit a pull request from your `new_feature` branch to this repository's `master` branch.

 * Note: any new commits pushed to the `new_feature` branch of your fork will automatically be included in the pull request, so make sure to only commit related changes to the same branch.

## Pull Request Notes
* Mark pull requests that are still being worked on by adding [WIP] before the title
    * When a pull request is ready to be reviewed remove the [WIP]
* Mark pull requests that need commenting/testing by others with [CR] before the title
    * [WIP] has higher precedence than [CR]. Feel free to remove [CR] when you feel you got enough information

## Learning git

[Git](https://www.git-scm.com/) is a powerful tool, but it's also complex and can be difficult to use for someone not used to it.

We recommend checking out [Learn Git Branching](https://pcottle.github.io/learnGitBranching/) in order to get acquainted with the concepts and commands that you will use in a typical coding session.


## Frequently Asked Questions

#### Why does `git pull --ff-only` result in an error?

If `git pull --ff-only` shows an error, it means that you've committed directly to your local `master` branch. To fix this, we create a new branch with these commits, find the point at which we diverged from `upstream/master`, and then reset `master` to that point.

    $ git pull --ff-only upstream master
    From https://bitbucket.org/lastmikoi/caelum-incognitum.git
     * branch            master     -> FETCH_HEAD
    fatal: Not possible to fast-forward, aborting.
    $ git branch new_branch master          # mark the current commit with a tmp branch
    $ git merge-base master upstream/master
    cc31d0... # the last commit before we committed directly to master
    $ git reset --hard cc31d0....
    HEAD is now at cc31d0... ...

Now that `master` has been cleaned up, we can easily pull from `upstream/master`, and then continue working on `new_branch`.

    $ git pull --ff-only upstream master
    # gets changes from the "upstream" remote for the matching branch, in this case "master"
    $ git checkout new_branch
